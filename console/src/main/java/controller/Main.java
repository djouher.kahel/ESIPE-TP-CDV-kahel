package controller;

import entity.Fibonacci;

import java.util.Scanner;

import org.apache.log4j.Logger;

/**
 * @author kahel
 */
public class Main {

    private static Logger logger = Logger.getLogger(Main.class);

    public static void main(String[] args) {
        String result;
        Fibonacci f = new Fibonacci();
        System.out.println("Souhaitez-vous entrer un nombre o ou n : ");
        Scanner sc1 = new Scanner(System.in);
        String str = sc1.next();
        if (str.equals("o")) {
            logger.info("L'utilisateur veut saisir le nombre.");
            System.out.println("Saisir un nombre :");
            Scanner sc = new Scanner(System.in);
            int i = sc.nextInt();
            logger.info("L'utilisateur a saisit le nombre : " + i);
            result = f.afficherN(i);
            System.out.println(result);
            logger.info("Le résultat est : " + result);
        } else {
            result = f.afficherN(5);
            System.out.println(result);
            logger.info("L'utilisateur a choisi le mode par défaut et le résultat est : " + result);
        }
    }

}
