package entity;

/**
 * @author kahel
 */
public class Fibonacci {

    public Fibonacci() {

    }

    public String afficherN(int n) {
        int cur = 0;
        int curApres = 1;
        String suiteF = "";
        suiteF = suiteF + cur + "; " + curApres + "; ";
        int attente;
        if (n == 1)
            return " " + cur;
        if (n == 2)
            return cur + "; " + curApres + "; ";

        for (int i = 2; i < n; i++) {
            attente = curApres;
            curApres = cur + curApres;
            cur = attente;
            suiteF = suiteF + " " + curApres + "; ";
        }
        return suiteF;
    }

}
