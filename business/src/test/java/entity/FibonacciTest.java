package entity;

import org.junit.Assert;
import org.junit.Test;

public class FibonacciTest {

    @Test
    public void afficherNRetourneValeurCorrecte() {
        Assert.assertEquals(" 0", new Fibonacci().afficherN(1));
        Assert.assertEquals("0; 1; ", new Fibonacci().afficherN(2));
    }

    @Test
    public void afficherNFonctionneAvecValeurNegative() {
        Assert.assertEquals("0; 1; ", new Fibonacci().afficherN(-2));
    }

}